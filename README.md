# snippets
snippet which helps me in my daily work

## ansible

`ansible-playbook playbook.yml -i hosts`
to install my tools locally

## docker

### docker registry
`docker images`

`docker login`

`docker tag imageid your-login/docker-demo`

`docker push your-login/docker-demo`

## minikube

`minikube start/stop`

`minikube dashboard`

`kubectl create -f first-app/helloworld.yml`

`kubectl describe poid nodehehello-minikube-7844bdb`

`kubectl port-forward hello-minikube-7844bdb9c6-l9n5c local_machine_port:container_port`

or

`kubectl expose pod nodehelloworld.example.com --type=NodePort --name nodehelloworld-service`

### cmds

`minikube service nnodehelloworld-service --url` -> returns the ip-address:port __http://192.168.99.100:30795__

`kubectl get serivce` --> `nodehelloworld-service   NodePort   10.104.80.146    <none>   3000:30795/TCP   11m`
  
`kubectl get pod`  --> `nodehelloworld.example.com        1/1       Running   0     37m`

`kubectl attach nodehelloworld.example.com` --> attaches the pod and shows the logs

`kubectl exec nodehelloworld.example.com --ls /app` --> executes the command in that container

`kubectl describe service nodehelloworld-service` --> shows service information

`kubectl describe pod helloworld-controller-4qfbl` --> shows pods informationn

`kubectl create -f first-app/helloworld.yml` --> creates what is in the file, Pod, Service, ReplicaController etc.

